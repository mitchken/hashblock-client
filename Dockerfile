FROM node:16
WORKDIR /usr/src/app
COPY package*.json ./

RUN npm i

COPY . ./
RUN npm run build
WORKDIR /usr/src/app

RUN npm i --production

ENTRYPOINT ["npm", "start"]
