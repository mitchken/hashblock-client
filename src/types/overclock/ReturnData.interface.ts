import { GPUOverClock } from './GPUOverclock.interface';

export interface ReturnDataInterface {
  overclocks?: GPUOverClock[]
}