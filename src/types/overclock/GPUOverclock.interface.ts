export interface GPUOverClock {
  index: number;
  absoluteCoreClock: number;
  memoryOffset: number;
  powerLimit: number;
  temperature: number;
}