export interface GPUInterface {
  index: number;
  name: string;
}

export interface GPUInterfaceWithStats {
  index: number;
  name: string;
  temperature: number;
  fanSpeed: number
}