import { CPUInterface } from './CPU.interface';
import { GPUInterface } from './GPU.interface';

export interface NodeInterface {
  cpu: CPUInterface;
  memoryInGB: number;
  gpus: GPUInterface[];
  hostname: string;
  ipAddress: string;
  version: string;
}