export interface CPUInterface {
  model: string;
  cores: number;
}