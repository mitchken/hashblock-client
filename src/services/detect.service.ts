import * as os from 'os';

import { inject, injectable } from 'inversify';

import { CPUInterface, GPUInterface, GPUInterfaceWithStats, NodeInterface } from '../types';

import { ExecService } from './exec.service';

@injectable()
export class DetectService {
  cpu: CPUInterface;
  memoryInGB: number;
  hostname: string;
  ipAddress: string;
  gpus: GPUInterface[];
  gpuInfos: GPUInterfaceWithStats[] = [];
  loadingDone: boolean = false;
  version: string;

  constructor(@inject(ExecService) private execService: ExecService) {}

  getSystem(): NodeInterface {
    if (!this.loadingDone) return;
    return {
      cpu: this.cpu,
      memoryInGB: this.memoryInGB,
      gpus: this.gpus,
      hostname: this.hostname,
      ipAddress: this.ipAddress,
      version: this.version,
    };
  }

  // memory: MemoryInterface;
  async checkSystem() {
    this.cpu = this.checkCpu();
    this.memoryInGB = this.checkMemory();
    this.version = this.checkVersion();
    this.ipAddress = await this.getIP();
    this.hostname = await this.getHostName();
    this.gpus = await this.checkGpus();
    this.loadingDone = true;
    void this.updateGpus();
  }

  checkCpu() {
    const cores = os.cpus();
    const [firstCore] = cores;
    const amountOfCores = cores.length;
    return {
      model: firstCore.model.trim(),
      cores: amountOfCores,
    };
  }

  checkMemory(): number {
    return Math.round((os.totalmem() / 1024 / 1024 / 1024) * 10) / 10;
  }

  async checkGpus(): Promise<GPUInterface[]> {
    const gpus = await this.execService.run('nvidia-smi -L');
    const parsedGpus: string[] = gpus.stdout
      .split('\n')
      .map((x) => x.split('(')[0].trim())
      .filter((x) => x.length > 10);
    const numberedGpus = parsedGpus.map((x) => {
      const split = x.split(':');
      return { index: Number(split[0].split(' ')[1]), name: split[1].trim() };
    });
    return numberedGpus;
  }

  async updateGpus(): Promise<void> {
    const lines = await this.execService.run('nvidia-smi -q');
    const cleanedLines = lines.stdout.split('\n').map((l) => l.replace(/(\r)/g, '').replace(/,/g, '').trim());
    const perGpu: { GPUNumber: number; currentSet: string[] }[] = [];
    let currentSet = [];
    let GPUNumber = -1;
    cleanedLines.forEach((l) => {
      const newGPU: boolean = l.includes('GPU 00000000');
      if (newGPU) {
        // throw away headers before first gpu
        if (GPUNumber >= 0) perGpu.push({ GPUNumber, currentSet });
        GPUNumber += 1;
        currentSet = [];
      }
      currentSet.push(l);
    });
    // push last gpu to the perGpuSet
    if (GPUNumber >= 0) perGpu.push({ GPUNumber, currentSet });
    for (const gpu of perGpu) {
      const temperature = Number(
        gpu.currentSet
          .find((cs) => cs.includes('GPU Current Temp'))
          .match(/\d+\sC$/)[0]
          .match(/\d+/)[0] || -1,
      );
      const fanSpeed = Number(
        gpu.currentSet
          .find((cs) => cs.includes('Fan Speed'))
          .match(/\d+\s%$/)?.[0]
          .match(/\d+/)?.[0] || -1,
      );
      const gpuDefinition = this.gpus.find((x) => x.index === gpu.GPUNumber);
      this.gpuInfos[gpu.GPUNumber] = { temperature, fanSpeed, ...gpuDefinition };
    }
  }

  async getHostName(): Promise<string> {
    const { stdout } = await this.execService.run('hostname');
    return stdout;
  }

  async getIP(): Promise<string> {
    const { stdout } = await this.execService.run('hostname -I');
    return stdout.split(' ')[0];
  }

  checkVersion(): string {
    return process.env.npm_package_version;
  }
}
