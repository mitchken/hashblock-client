import { inject, injectable } from 'inversify';
import { ExecService } from './exec.service';
import { GPUOverClock } from '../types';

@injectable()
export class OverclockService {
  screenSetup: boolean = false;

  constructor(@inject(ExecService) private execService: ExecService) {}

  async setupScreen() {
    if (!this.screenSetup) {
      console.log('setting up screen');
      await this.execService.run(
        `
        sudo X :1 & \n
        export DISPLAY=:1;`,
        false,
      );
      this.screenSetup = true;
    }
  }

  async setOverclocks(gpuOverClocks: GPUOverClock[]) {
    try {
      await this.setupScreen();
      for (const gpuOverClock of gpuOverClocks) {
        await this.setOverclock(gpuOverClock);
        console.log(`applied overclocks for gpu ${gpuOverClock.index}`);
      }
    } catch (e) {
      console.error('something went wrong while applying overclocks', e);
    }
  }

  async setOverclock({ index, absoluteCoreClock, powerLimit, temperature, memoryOffset }: GPUOverClock) {
    // enable pm (persistance mode)
    await this.execService.run(`nvidia-smi -i ${index} -pm 1`, true);
    //set memory
    if (memoryOffset) {
      await this.execService.run(`DISPLAY=:1 sudo nvidia-settings -a [gpu:${index}]/GPUMemoryTransferRateOffset[3]=${memoryOffset}`, false);
      await this.execService.run(`DISPLAY=:1 sudo nvidia-settings -a [gpu:${index}]/GPUMemoryTransferRateOffsetAllPerformanceLevels=${memoryOffset}`, false);
    }
    //set core
    if (absoluteCoreClock) {
      await this.execService.run(`nvidia-smi -i ${index} --lock-gpu-clocks= ${absoluteCoreClock}`, true);
    }
    //set pl
    if (powerLimit) {
      await this.execService.run(`nvidia-smi -i ${index} --power-limit ${powerLimit}`, true);
    }
    //set pl
    if (temperature) {
      const x = await this.execService.run(`nvidia-smi -i ${index} --gpu-target-temp ${temperature}`, true);
    }
  }
}
