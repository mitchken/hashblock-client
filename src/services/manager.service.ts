import { inject, injectable } from 'inversify';
import { VariablesService } from './variables.service';
import { DetectService } from './detect.service';
import { LoggingService } from './logging.service';

@injectable()
export class ManagerService {
  constructor(
    @inject(VariablesService) private variablesService: VariablesService,
    @inject(DetectService) private detectService: DetectService,
    @inject(LoggingService) private loggingService: LoggingService,
  ) {}

  async init() {
    this.runLoop();
    await this.detectService.checkSystem();
  }

  queueLoop() {
    setTimeout(this.runLoop.bind(this), this.variablesService.getVariable('iteration_duration_in_seconds') * 1000);
  }

  async runLoop(): Promise<void> {
    this.loggingService.log();
    this.detectService.updateGpus();
    this.queueLoop();
  }
}
