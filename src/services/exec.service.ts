import { exec } from 'child_process';

import { injectable } from 'inversify';

@injectable()
export class ExecService {
  async run(command: string, sudo: boolean = false): Promise<{ stdout: string; stderr: string }> {
    return new Promise((resolve) => {
      const cmdToRun = `${sudo ? `sudo ` : ''}${command}`;
      const runningCommand = exec(cmdToRun);
      const stdout = [];
      const stderr = [];
      runningCommand.stdout.on('data', (d) => stdout.push(d));
      runningCommand.stderr.on('data', (d) => stderr.push(d));
      runningCommand.on('exit', (exit) => {
        if (stderr?.length > 0) console.warn('cmd', cmdToRun, 'stderr', stderr);
        resolve({ stdout: stdout.join(), stderr: stderr.join() });
      });
    });
  }
}
