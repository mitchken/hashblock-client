import { injectable } from 'inversify';

@injectable()
export class VariablesService {
  variables = {
    iteration_duration_in_seconds: 10,
  };

  public getVariable(field: string) {
    return this.variables[field];
  }

  public setVariables(field: string, value) {
    this.variables[field] = value;
  }

  public getAllVariables(field: string) {
    return this.variables;
  }
}
