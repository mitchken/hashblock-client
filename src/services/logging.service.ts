import { inject, injectable } from 'inversify';
import axios from 'axios';
import dayjs from 'dayjs';

import { DetectService } from './detect.service';
import { OverclockService } from './overclock.service';

@injectable()
export class LoggingService {
  systemLogged: boolean = false;
  overclocksRetrieved: boolean = false;

  constructor(@inject(DetectService) private detectService: DetectService, @inject(OverclockService) private overclockService: OverclockService) {}

  async log() {
    try {
      const system = this.getSystemLog();
      const res = await axios.post(`${process.env.MANAGER_URL}/log/${process.env.INSTANCE_ID}`, {
        ...(system && { system }),
        gpuInfos: this.detectService.gpuInfos,
        sentAt: dayjs().toISOString(),
        returnOverclocks: this.systemLogged && !this.overclocksRetrieved,
      });
      if (system && !this.systemLogged) this.systemLogged = true;
      if (res.data.overclocks && !this.overclocksRetrieved) {
        this.overclocksRetrieved = true;
        console.log('setting overclocks', res.data.overclocks);
        await this.overclockService.setOverclocks(res.data.overclocks);
      }
    } catch (e) {
      console.error(e);
      console.error('Failed to create log', e.message);
    }
  }

  getSystemLog() {
    let system;
    if (!this.systemLogged) {
      system = this.detectService.getSystem();
    }
    return system;
  }
}
