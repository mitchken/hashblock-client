import { ContainerModule, interfaces } from 'inversify';
import { DetectService, ExecService, LoggingService, ManagerService, OverclockService, VariablesService } from '../services';

export const services = new ContainerModule((bind: interfaces.Bind): void => {
  bind<DetectService>(DetectService).toSelf().inSingletonScope();
  bind<ManagerService>(ManagerService).toSelf().inSingletonScope();
  bind<OverclockService>(OverclockService).toSelf().inSingletonScope();
  bind<VariablesService>(VariablesService).toSelf().inSingletonScope();
  bind<ExecService>(ExecService).toSelf().inSingletonScope();
  bind<LoggingService>(LoggingService).toSelf().inSingletonScope();
});
