import { Container } from 'inversify';

import { services } from './services';

const container = new Container();
container.load(services);

export { container };
