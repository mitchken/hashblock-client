import 'reflect-metadata';

import { container } from './ioc/container';
import { App } from './App';

(async (): Promise<void> => {
  try {
    const app = new App(container);
    await app.start();
  } catch (e) {
    console.error(e);
  }
})();