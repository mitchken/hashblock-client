import { interfaces } from 'inversify';
import { ManagerService } from './services';

export class App {
  constructor(private container: interfaces.Container) {
    console.log('Booted');
  }

  async start() {
    console.log('Starting');
    await this.container.get<ManagerService>(ManagerService).init();
    console.log('Started');
  }
}